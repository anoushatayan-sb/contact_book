build:
	make build-docker
	make migrate
	make collectstatic

build-docker:
	docker-compose build
	docker-compose up --no-start dbp
	docker-compose start dbp

run-project:
	docker-compose up web dbp

run-celery:
	docker-compose up celery

manage-python:
	docker-compose run --rm web python manage.py $(command)

makemigrations: command=makemigrations
makemigrations: manage-python

migrate: command=migrate
migrate: manage-python

shell: command=shell
shell:  manage-python

collectstatic: command=collectstatic --noinput
collectstatic: manage-python

test:
	docker-compose run web coverage erase
	docker-compose run web coverage run manage.py test
	docker-compose run web coverage report

lint:
	docker-compose run --rm web flake8

