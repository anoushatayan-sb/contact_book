import os

import pandas as pd


def read_excel(excel_file) -> pd.DataFrame:
    _, extension = os.path.splitext(excel_file.file.name)
    df = pd.read_excel(
        excel_file.file.read(), dtype=str, engine='openpyxl' if extension == '.xlsx' else None)
    return df
