from contact_book.base_conf import Base
from contact_book.stage_conf import Stage
from contact_book.prod_conf import Prod

try:
    from contact_book.local_conf import Local
except ImportError:
    pass
