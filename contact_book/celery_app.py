import os

import celery
from configurations import importer

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'contact_book.settings')
os.environ.setdefault('DJANGO_CONFIGURATION', 'Local')
importer.install()

app = celery.Celery('contact_book', broker='redis://redis:6379/0')
app.autodiscover_tasks()
app.conf.redbeat_redis_url = os.getenv('REDIS_URL', 'redis://redis:6379/0')
app.conf.broker_pool_limit = 1
app.conf.broker_heartbeat = None
app.conf.broker_connection_timeout = 30
app.conf.worker_prefetch_multiplier = 1
