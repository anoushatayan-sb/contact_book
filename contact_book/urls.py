from django.contrib import admin
from django.urls import path

from contacts.views import ContactsUploadView
from django.views.generic import TemplateView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', ContactsUploadView.as_view(), name='contacts_upload'),
    path('uploaded/', TemplateView.as_view(template_name='uploaded.html'), name='uploaded'),
]
