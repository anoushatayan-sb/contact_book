from django.contrib import messages
from django.urls import reverse_lazy
from django.views.generic.edit import FormView

from contacts.forms import ContactsUploadForm


class ContactsUploadView(FormView):
    template_name = 'index.html'
    form_class = ContactsUploadForm

    def form_valid(self, form):
        form.save()
        self.success_url = reverse_lazy('uploaded')
        messages.success(self.request, "Thank you! Your upload is underway.")
        return super().form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, "Something went wrong...")
        return super().form_invalid(form)
