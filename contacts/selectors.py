import logging
from datetime import timedelta

from django.utils import timezone
from pandas import DataFrame

from .models import Contact

logger = logging.getLogger('contacts')


def preprocess_contact_data(*, df: DataFrame) -> DataFrame:
    """
    Add datetime for "created" and
    Exclude contacts:
        1. without phone number;
        2. with the same email or phone_number existing in the dataframe;
        3. with the same email or phone_number uploaded during last 3 minutes.
    """

    now = timezone.now()
    range_min = now - timedelta(minutes=3)

    exclude_qs = list(Contact.objects.filter(created__range=[range_min, now]).values_list('phone_number', 'email'))
    df.dropna(subset=['Phone Number'], inplace=True)
    df.drop_duplicates(subset='Phone Number', keep='last', inplace=True)
    df.drop_duplicates(subset='Email Address', keep='last', inplace=True)
    df = df[
        ~(df['Phone Number'].isin([i[0] for i in exclude_qs]) | df['Email Address'].isin([i[1] for i in exclude_qs]))
    ]
    df['created'] = now
    df = df.fillna('')
    return df
