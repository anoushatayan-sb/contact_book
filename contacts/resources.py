from import_export import resources

from contacts.models import Contact


class ContactResource(resources.ModelResource):
    class Meta:
        model = Contact
        skip_unchanged = True
        report_skipped = True
        exclude = ('id',)
        import_id_fields = ('name', 'phone_number', 'email', 'created')
