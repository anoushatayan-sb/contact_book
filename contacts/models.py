from celery import current_app
from django.core.validators import FileExtensionValidator
from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class ContactData(models.Model):
    file = models.FileField(validators=[FileExtensionValidator(['xls', 'xlsx'])],
                            upload_to='contacts/%Y/%m/%d')
    uploaded = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = 'Contact Data'

    def __str__(self):
        return self.file.name

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if self.pk:
            current_app.send_task('contacts.tasks.import_contacts', args=(self.pk,))


class Contact(models.Model):
    name = models.CharField(max_length=100, blank=True)
    phone_number = PhoneNumberField()
    email = models.EmailField(blank=True)
    created = models.DateTimeField()
