from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms

from contacts.models import ContactData


class ContactsUploadForm(forms.ModelForm):
    class Meta:
        model = ContactData
        fields = ['file']

    helper = FormHelper()
    helper.add_input(Submit('submit', 'Submit', css_class='btn-primary btn-block rounded-pill shadow'))
    helper.form_method = 'POST'
    helper.form_show_labels = False
