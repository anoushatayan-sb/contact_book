import os
import shutil

from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from django.urls import reverse


class TestViews(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.url = reverse('contacts_upload')

    def test_detail_view(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_upload_redirection(self):
        data = {
            'file': SimpleUploadedFile(
                'contacts.xls', b'contact data', content_type='application/vnd.ms-excel'
            ),
        }
        response = self.client.post(self.url, data=data, follow=True)

        self.assertRedirects(response, reverse('uploaded'), status_code=302, target_status_code=200)
        self.assertEqual(list(response.context.get('messages'))[0].message, 'Thank you! Your upload is underway.')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        shutil.rmtree(os.path.join(settings.BASE_DIR, 'media'))
