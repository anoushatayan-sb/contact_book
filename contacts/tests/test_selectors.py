import numpy as np
from django.test import TestCase
from pandas import DataFrame

from contacts.selectors import preprocess_contact_data
from .factories import ContactFactory


class TestSelectors(TestCase):
    def setUp(self) -> None:
        self.contact = ContactFactory()

    def test_preprocess_contact_data(self):
        columns = ['Name', 'Phone Number', 'Email Address']
        data = [
            ['Bob', np.nan, 'bob.marley@example.com'],  # empty Phone Number
            ['Linda', '+37455666666', 'john.doe@example.com'],  # the same Email Address as for John
            ['John', '+37455888888', 'john.doe@example.com'],
            ['Alice', '+37455777777', 'alice.lopez@example.com'],  # the same Phone Number as for David
            ['David', '+37455777777', 'david.ramirez@example.com'],
            [self.contact.name, self.contact.phone_number, ''],  # the Phone Number exists in DB
            [self.contact.name, '+37455999999', self.contact.email]  # the Email Address exists in DB

        ]

        result = preprocess_contact_data(df=DataFrame(columns=columns, data=data))
        self.assertListEqual(list(result['Name']), ['John', 'David'])
        self.assertListEqual(list(result['Phone Number']), ['+37455888888', '+37455777777'])
        self.assertListEqual(list(result['Email Address']), ['john.doe@example.com', 'david.ramirez@example.com'])
