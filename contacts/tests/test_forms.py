from django.test import TestCase
from contacts.forms import ContactsUploadForm
from django.core.files.uploadedfile import SimpleUploadedFile


class TestForms(TestCase):
    def test_contacts_upload_form(self):
        # empty data
        form = ContactsUploadForm(data={})
        self.assertFalse(form.is_valid())

        # wrong file format
        file_data = {
            'file': SimpleUploadedFile(
                'contacts.jpg', b'contact data', content_type='image/jpeg'
            ),
        }
        form = ContactsUploadForm(data={}, files=file_data)
        self.assertFalse(form.is_valid())

        # excepted file format
        file_data = {
            'file': SimpleUploadedFile(
                'contacts.xls', b'contact data', content_type='application/vnd.ms-excel'
            ),
        }
        form = ContactsUploadForm(data={}, files=file_data)
        self.assertTrue(form.is_valid())
