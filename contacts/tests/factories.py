from django.utils import timezone
from factory.django import DjangoModelFactory


class ContactFactory(DjangoModelFactory):
    name = 'Dua'
    phone_number = '+37494778888'
    email = 'dua.lipa@example.com'
    created = timezone.now()

    class Meta:
        model = 'contacts.Contact'
