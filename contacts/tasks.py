import logging

import tablib
from celery import shared_task

from contacts.models import ContactData
from contacts.resources import ContactResource
from lib.utils import read_excel
from .selectors import preprocess_contact_data

logger = logging.getLogger('contacts')


@shared_task
def import_contacts(pk: int):
    """
    Import Contacts to db from excel file.
    """

    excel_file = ContactData.objects.get(pk=pk)
    df = read_excel(excel_file)

    if list(df.columns) != ['Name', 'Phone Number', 'Email Address']:
        logger.error(f'ContactData file with id {pk} does not have correct column structure. It is not imported.')
        return
    df = preprocess_contact_data(df=df)

    df.columns = ContactResource.Meta.import_id_fields
    dataset = tablib.Dataset().load(df, format='df')
    cr = ContactResource()
    result = cr.import_data(dataset, dry_run=True)
    if not result.has_errors():
        cr.import_data(dataset, dry_run=False)
    else:
        logger.warning(f'Could not import data from ContactData file with id {pk}')
