import os

from django.contrib import admin

from .admin_mixins import ReadOnlyMixin
from .models import ContactData, Contact


@admin.register(ContactData)
class ContactDataAdmin(ReadOnlyMixin, admin.ModelAdmin):
    readonly_fields = ('file', 'uploaded')
    list_display = ('file_name', 'uploaded')

    def file_name(self, obj):
        return os.path.split(obj.file.name)[-1]


@admin.register(Contact)
class ContactAdmin(ReadOnlyMixin, admin.ModelAdmin):
    list_display = ('name', 'phone_number', 'email', 'created')
    readonly_fields = ('created',)
